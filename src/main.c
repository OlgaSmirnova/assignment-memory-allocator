#define _DEFAULT_SOURCE
#include "test.h"

#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void* heap;
void debug(const char* fmt, ... );
static struct block_header* start_block;
static void* allocated_block;
static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
            struct block_header, contents));
}


void test_init(){
        heap = heap_init(10000);
        if (heap == NULL) {
            err ("err: error in heap initialization\n");
        } else {
            debug_heap (stderr, heap);
        }
        start_block = (struct block_header*) heap;

    }

    void test_allocate(){
        debug("\n ---Start one block free test ---\n");
        void* mem = _malloc(1000);
        if (mem == NULL)
            err("memory allocate test failed: Null pointer.");
        if(start_block->capacity.bytes != 1000)
            err("memory allocate test failed");
        if(start_block->is_free)
            err("memory allocate test failed");
        debug("\nMemory allocate test passed\n");
        debug_heap(stderr, heap);
        allocated_block = mem;
    }

    void test_free_one(){
        debug("--- Start one block free test ---\n");
        void* test2_mem = _malloc(1000);
        _malloc(1000);
        _free(test2_mem);
        debug_heap(stderr, heap);
        if (!((struct block_header*) (((uint8_t*) test2_mem)-offsetof(struct block_header, contents)))->is_free) {
            err("err: one block free test failed\n");
        }
        debug ("One block free test passed");
    }

    void test_free_two(){
        debug("\n --- Start two blocks free test ---\n");
        void* first = _malloc(1000);
        void* second = _malloc(1000);
        debug("\ntwo blocks free test failed\n");
        debug_heap(stderr, heap);
        _free(first);
        _free(second);
        if(!start_block->is_free)
            err("two blocks free test failed");
        debug("\nTwo block free test passed\n");
        debug_heap(stderr, heap);
    }

    void test_heap_grow_extending(){
        debug("\n --- Start new region extends old test ---\n");
        const size_t SZ = 2 * 10000;
        void* mem = _malloc(SZ);
        debug_heap(stderr, heap);
        if (mem == NULL)
            err("new region extends old test failed");
        struct block_header *last = block_get_header(mem);
        if(last->capacity.bytes != SZ)
            err("new region extends old test failed");
        if(last->is_free)
            err("new region extends old test failed");
        _free(mem);
        if(!last->is_free)
            err("new region extends old test failed");
        debug("\nNew region extends old test passed\n");
        debug_heap(stderr, heap);
    }

struct block_header* last_block() {
    for (struct block_header* last = start_block; ; last = last->next) {
        if (last->next == NULL) {
            return last;
        }
    }
}


void test_heap_grow(){
        debug ("--- Start new region another place test ---\n");

        struct block_header* last = last_block();

        void* test5_taken_mem_addr = last + size_from_capacity(last->capacity).bytes;

        void* test5_taken_mem = mmap( (uint8_t*) (getpagesize() * ((size_t) test5_taken_mem_addr / getpagesize()
                                                                   + (((size_t) test5_taken_mem_addr % getpagesize()) > 0))),1000, PROT_READ | PROT_WRITE,
                                      MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);

        debug("test taken mem addr: %p\n", test5_taken_mem);

        void* test5_mem = _malloc(40000);

        debug_heap(stderr, heap);

        if (test5_mem == start_block->next || test5_mem == test5_taken_mem ||
            ((struct block_header*) (((uint8_t*) test5_mem)-offsetof(struct block_header, contents)))->capacity.bytes != 40000 ||
                                                                                                       ((struct block_header*) (((uint8_t*) test5_mem)-offsetof(struct block_header, contents)))->is_free) {
            err("err: new region another place test failed\n");
        }
        debug ("New region another place test passed\n");
    }
int main() {
    test_init();
    test_allocate();
    test_free_one();
    test_free_two();
    test_heap_grow_extending();
    test_heap_grow();

    return 0;
}


