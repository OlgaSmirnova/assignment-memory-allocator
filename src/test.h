
#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

void test_init();

void test_allocate();

void test_free_one();

void test_free_two();

void test_heap_grow_extending();

void test_heap_grow();


#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
